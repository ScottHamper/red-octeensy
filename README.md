RedOcteensy
===========
Software for [the Teensy-LC microcontroller](https://www.pjrc.com/teensy/teensyLC.html) to translate wire signals from a RedOctane PXMTP DDR dance pad upgraded with force-sensing resistors into generic gamepad inputs. Uses the following control layout:

| Button         | Gamepad Mapping | Teensy Pin |
|----------------|-----------------|------------|
| Up-Right Arrow | Button 1        | 19 (A5)    |
| Up-Left Arrow  | Button 2        | 16 (A2)    |
| Up Arrow       | Button 3        | 17 (A3)    |
| Right Arrow    | Button 4        | 14 (A0)    |
| Down Arrow     | Button 5        | 18 (A4)    |
| Left Arrow     | Button 6        | 15 (A1)    |

Visit [the project website](https://www.scotthamper.com/red-octeensy) for more information, including a complete parts list, PXMTP pinout, and build notes.

For the original version of this code that works with an unmodified PXMTP pad and presents the pad as an Xbox 360 controller, see [version 1.0](https://gitlab.com/ScottHamper/red-octeensy/-/tree/v1.0.0).

Dependencies
------------
- [Teensy Board Files for Arduino XInput Library v1.1.1](https://github.com/dmadison/ArduinoXInput_Teensy)

Usage
-----
Once the code has been flashed to the Teensy, it's a 100% Plug and Play experience. The Teensy will appear to the OS as a gamepad and can have its inputs bound in [StepMania](https://www.stepmania.com/) and/or [RetroArch](https://www.retroarch.com/).

To adjust sensitivity, modify the `TRIGGER_THRESHOLD` constant. I found that a single value worked well for all buttons, but your mileage may vary!

License
-------
Copyright © 2022 Scott Hamper

This program is free software: you can redistribute it and/or modify it under the terms of the GNU
Affero General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/)>.
