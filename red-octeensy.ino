inline void translateButtonState(int pin, int button) __attribute__((always_inline));

const int PIN_LED = 13;
const int PIN_BUTTON_RIGHT = 14;
const int PIN_BUTTON_LEFT = 15;
const int PIN_BUTTON_UP_LEFT = 16;
const int PIN_BUTTON_UP = 17;
const int PIN_BUTTON_DOWN = 18;
const int PIN_BUTTON_UP_RIGHT = 19;

const int BUTTON_UP_RIGHT = 1;
const int BUTTON_UP_LEFT = 2;
const int BUTTON_UP = 3;
const int BUTTON_RIGHT = 4;
const int BUTTON_DOWN = 5;
const int BUTTON_LEFT = 6;

const int TRIGGER_THRESHOLD = 450;

void setup() {
  Joystick.useManualSend(true);

  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, HIGH);
}

void loop() {
  translateButtonState(PIN_BUTTON_RIGHT, BUTTON_RIGHT);
  translateButtonState(PIN_BUTTON_LEFT, BUTTON_LEFT);
  translateButtonState(PIN_BUTTON_UP_LEFT, BUTTON_UP_LEFT);
  translateButtonState(PIN_BUTTON_UP, BUTTON_UP);
  translateButtonState(PIN_BUTTON_DOWN, BUTTON_DOWN);
  translateButtonState(PIN_BUTTON_UP_RIGHT, BUTTON_UP_RIGHT);
  Joystick.send_now();
}

void translateButtonState(int pin, int button) {
  Joystick.button(button, analogRead(pin) >= TRIGGER_THRESHOLD);
}
